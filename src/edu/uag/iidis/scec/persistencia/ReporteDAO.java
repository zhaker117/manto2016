package edu.uag.iidis.scec.persistencia;

import org.hibernate.*;
import org.hibernate.type.*;
import org.hibernate.criterion.Example;


import edu.uag.iidis.scec.excepciones.ExcepcionInfraestructura;
import edu.uag.iidis.scec.modelo.Rol;
import edu.uag.iidis.scec.modelo.Reporte;
import edu.uag.iidis.scec.persistencia.hibernate.HibernateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;


public class ReporteDAO {

    private Log log = LogFactory.getLog(ReporteDAO.class);

    public ReporteDAO() {
    }

    public Collection buscarTodos()
            throws ExcepcionInfraestructura {

        Collection roles;

        if (log.isDebugEnabled()) {
            log.debug(">buscarTodos()");
        }

        try {
            roles = HibernateUtil.getSession()
                                    .createCriteria(Reporte.class)
                                    .list();
        } catch (HibernateException e) {
            if (log.isWarnEnabled()) {
                log.warn("<HibernateException");
            }
            throw new ExcepcionInfraestructura(e);
        }
        return roles;
    }


}
