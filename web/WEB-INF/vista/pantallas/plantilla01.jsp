<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
<html>
    <head>
        <title>Administrador de Personal</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" type="text/css" href="css/plantilla.css">
        <link rel="stylesheet" type="text/css" href="css/mensajes.css">
    </head>

    <body>
        <div id="container">
            <div id="header">
                <c:import url="/WEB-INF/vista/comun/banner.jsp" />
                <c:import url="/WEB-INF/vista/comun/barraMenu01.jsp" />
            </div>
            <div id="body">
                <c:import url="/WEB-INF/vista/pantallas/${param.c}" />
            </div>
            <div id="footer">

            </div>

    </body>
</html>
