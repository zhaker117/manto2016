package edu.uag.iidis.scec.servicios;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import edu.uag.iidis.scec.modelo.Rol;
import edu.uag.iidis.scec.excepciones.*;
import edu.uag.iidis.scec.persistencia.ReporteDAO;
import edu.uag.iidis.scec.persistencia.hibernate.*;

public class ManejadorReporteLugar {
    private Log log = LogFactory.getLog(ManejadorReporteLugar.class);
    private ReporteDAO dao;

    public ManejadorReporteLugar() {
        dao = new ReporteDAO();
    }


    public Collection listarRoles() {
        Collection resultado;

        if (log.isDebugEnabled()) {
            log.debug(">guardarUsuario(usuario)");
        }

        try {
            HibernateUtil.beginTransaction();
            resultado = dao.buscarTodos();
            HibernateUtil.commitTransaction();
            return resultado;         
        } catch (ExcepcionInfraestructura e) {
            HibernateUtil.rollbackTransaction();
            return null;
        } finally {
            HibernateUtil.closeSession();
        }
    }
}
