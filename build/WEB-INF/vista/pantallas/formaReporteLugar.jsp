    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
    <%@ taglib uri="/WEB-INF/vista/etiquetas/struts-html.tld" prefix="html" %>
    <div class="container">
      <h3>REPORTE Lugares</h3>
      <table class="table table-striped">
        <thead>
          <tr>
            <th align="center"> Estado</th>
            <th align="center"> Nombre</th>
            <!--th>Descripción</th>
            <th>población</th>
            <th>Coordenadas (GPS)</th-->
          </tr>
        </thead>
        <tbody>
            <c:forEach var="rol" items="${formaReporteLugar.roles}">
                <tr>
                    <td align="center" ><c:out value="${rol.nombre}"/></td>
                    <td align="center" ><c:out value="${rol.descripcion}"/></td>       
                </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>