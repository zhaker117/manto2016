package edu.uag.iidis.scec.persistencia;

import org.hibernate.*;
import org.hibernate.type.*;
import org.hibernate.criterion.Example;
//import org.hibernate.classic.*;


import edu.uag.iidis.scec.excepciones.ExcepcionInfraestructura;
import edu.uag.iidis.scec.modelo.Login;
import edu.uag.iidis.scec.persistencia.hibernate.HibernateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;


public class LoginDAO {

    private Log log = LogFactory.getLog(LoginDAO.class);

    public LoginDAO() {
    }


    public Login buscarPorId(Long idLogin, boolean bloquear)
            throws ExcepcionInfraestructura {

        Login Login = null;

        if (log.isDebugEnabled()) {
            log.debug(">buscarPorID(" + idLogin + ", " + bloquear + ")");
        }

        try {
            if (bloquear) {
                Login = (Login)HibernateUtil.getSession()
                                                .load(Login.class, 
                                                      idLogin, 
                                                      LockMode.UPGRADE);
            } else {
                Login = (Login)HibernateUtil.getSession()
                                                .load(Login.class,
                                                      idLogin);
            }
        } catch (HibernateException ex) {
            if (log.isWarnEnabled()) {
                log.warn("<HibernateException");
            }

            throw new ExcepcionInfraestructura(ex);
        }
        return Login;
    }


    public Collection buscarTodos()
            throws ExcepcionInfraestructura {

        Collection Logins;

        if (log.isDebugEnabled()) {
            log.debug(">buscarTodos()");
        }

        try {
            Logins = HibernateUtil.getSession()
                                    .createCriteria(Login.class)
                                    .list();
									
			  log.debug(">buscarTodos() ---- list	");									
        } catch (HibernateException e) {
            if (log.isWarnEnabled()) {
                log.warn("<HibernateException");
            }
            throw new ExcepcionInfraestructura(e);
        }
        return Logins;
    }



   /* public Collection buscarPorEjemplo(Login Login)
            throws ExcepcionInfraestructura {


        Collection Logins;
 
        if (log.isDebugEnabled()) {
            log.debug(">buscarPorEjemplo()");
        }

        try {
            Criteria criteria = HibernateUtil.getSession()
                                             .createCriteria(Login.class);
            Logins = criteria.add(Example.create(Login)).list();
        } catch (HibernateException e) {
            if (log.isWarnEnabled()) {
                log.warn("<HibernateException");
            }
            throw new ExcepcionInfraestructura(e);
        }
        return Logins;
    }
*/

    public void hazPersistente(Login Login)
            throws ExcepcionInfraestructura {

        if (log.isDebugEnabled()) {
            log.debug(">hazPersistente(Login)");
        }

        try {
            HibernateUtil.getSession().saveOrUpdate(Login);
        } catch (HibernateException e) {
            if (log.isWarnEnabled()) {
                log.warn("<HibernateException");
            }
            throw new ExcepcionInfraestructura(e);
        }
    }


    public void hazTransitorio(Login Login)
            throws ExcepcionInfraestructura {

        if (log.isDebugEnabled()) {
            log.debug(">hazTransitorio(Login)");
        }

        try {
            HibernateUtil.getSession().delete(Login);
        } catch (HibernateException e) {
            if (log.isWarnEnabled()) {
                log.warn("<HibernateException");
            }
            throw new ExcepcionInfraestructura(e);
        }
    }

    public boolean existeLogin(String usuario, String pass)
            throws ExcepcionInfraestructura {

        try {
            if (log.isDebugEnabled()) {
                log.debug("<<<Verificar Datos Login");
            }

			//String sql = "SELECT * FROM users WHERE nombrePrefijo='"+usuario+"' AND claveAcceso='"+pass+"'";
            String sql = "SELECT * FROM users WHERE nombrePrefijo=:matricula AND claveAcceso=:passwd";            
            //String sql = "from usuario where nombrePrefijo = :matricula and claveAcceso = :clave";
            //String sql = "SELECT u.idUsuario FROM users u WHERE u.nombrePrefijo= :name AND u.claveAcceso= :passwd";
        
            if (log.isDebugEnabled()) {
                log.debug(sql);
        	}

            Session paso = HibernateUtil.getSessionFactory().openSession();
			//Session paso = HibernateUtil.getSession();
            if (log.isDebugEnabled()) {
                log.debug(">>Abre Session factory Entra");
            }
			//Query query = paso.createQuery(sql);
            SQLQuery query = paso.createSQLQuery(sql);//.addEntity(Login.class);
            query.setParameter("matricula", usuario);
            query.setParameter("passwd", pass);
            
			if (log.isDebugEnabled()) {
           		log.debug("<<<<<<<<< set Parameters, before query list >>>>>");
        	}
			List results = query.list();
            if (log.isDebugEnabled()) {
                log.debug("<<<<<<<<< TAMANHO: "+results.size());
            }
            if(results.size() > 0){
                paso.close();
                return true;
            }
            paso.close();
            return false;

        } catch (HibernateException ex) {
            if (log.isWarnEnabled()) {
                log.warn("<<<HibernateException:"+ ex.getMessage() +">>>");
            }
            throw new ExcepcionInfraestructura(ex);
        }
    }

}
