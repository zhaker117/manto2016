package edu.uag.iidis.scec.modelo;

import java.io.Serializable;
import java.util.*;


/**
 * Esta clase es usada para representar un rol de un
 * usuario.
 *
 * <p><a href="Estado.java.html"><i>Ver codigo fuente</i></a></p>
 *
 * @author <a href="mailto:vramos@uag.mx">Victor Ramos</a>
 * @version 1.0
 */
public class Recomendacion extends ClaseBase
        implements Serializable {

    private Long id;
    private String usuario;
    private String fecha;
    private String lugar;
    private String estado;
    private String comentario;
    private String calificacion;


    public Recomendacion() {
    }

    public Recomendacion(Long id){
        this.id = id;
    }

    public Recomendacion(String usuario, String fecha, String lugar, String estado, String comentario, String calificacion){
        this.usuario=usuario;
        this.fecha=fecha;
        this.lugar=lugar;
        this.estado=estado;
        this.comentario=comentario;
        this.calificacion=calificacion;
    }

    /**
     * Regresa el id del rol.
     * @return Long
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Establece el id del estado.
     * @return void
     */
    public void setId(Long id) {
        this.id = id;
    }


    /**
     * Regresa el nombre del estado.
     * @return String
     */
    public String getUsuario() {
        return this.usuario;
    }

    /**
     * Establece el nombre del estado.
     * @return void
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * Regresa la descripcion del estado.
     * @return String
     */
    public String getFecha() {
        return this.fecha;
    }

    /**
     * Establece la descripcion del estado.
     * @return void
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /*
    *Regresa la poblacion del lugar
    *@return Long
    */
    public String getLugar(){
      return this.lugar;
    }

    /*
    *Establece la poblacion del lugar
    *@return void
    */
    public void setLugar(String lugar){
      this.lugar = lugar;
    }

    /*
    *Regresa las coordenadas del lugar
    *@return String
    */

    public String getEstado(){
      return this.estado;
    }

    /*
    *Establece las coordenadas del lugar
    *@return void
    */
    public void setEstado(String estado){
      this.estado = estado;
    }

    /*
    *Regresa el estado del lugar
    *@return String
    */
    public String getComentario(){
      return this.comentario;
    }

    /*
    *Establece el estado del lugar
    *@return void
    */
    public void setComentario(String comentario){
      this.comentario = comentario;
    }

    public String getCalificacion(){
      return this.calificacion;
    }

    public void setCalificacion(String calificacion){
      this.calificacion = calificacion;
    }
}
