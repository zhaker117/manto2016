	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
    <%@ taglib uri="/WEB-INF/vista/etiquetas/struts-html.tld" prefix="html" %>

<html>
    <head>
        <title>Login</title>
        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="css/login.css">
        <link rel="stylesheet" type="text/css" href="css/mensajes.css">
    </head>
    
    <body>
        <!--section class="inicio">
            <c:import url="/WEB-INF/vista/pantallas/${param.c}" />
        </section-->
        <div class="wrapper">
            <div class="container">
                <h1>Welcome</h1>
                <form id="forma" action="Loginp.do" method="post">
			        <div>
			            <html:errors />
			        </div>
			        <div class="form-login">
	                    <input type="text" 
	                    		placeholder="ID"
	                    		name= "nombrePrefijo"
			                   	maxlength="100">
	                    <input type="password" 
	                    		placeholder="PASSWORD"
	                    		name= "claveAcceso"
			                   maxlength="100">
			        </div>
			        <input type="submit" 
			              name="submit"
			              class="btn btn-info" 
			              value="Auntentificar"/>
                </form>
            </div>
        </div>
    </body>
</html>