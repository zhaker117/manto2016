package edu.uag.iidis.scec.modelo;

import java.io.Serializable;
import java.util.*;

public class Login extends ClaseBase 
        implements Serializable {

    private Long id;
    private String nombrePrefijo;
    private String claveAcceso;

    public Login() {
    }

    public Login(Long id){
        this.id = id;
    }

    public Login(String nombrePrefijo, String claveAcceso){
        this.nombrePrefijo=nombrePrefijo;
        this.claveAcceso=claveAcceso;
    }
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    /**
     * Regresa el nombrePrefijo del rol.
     * @return String
     */
    public String getNombrePrefijo() {
        return this.nombrePrefijo;
    }
    public void setNombrePrefijo(String nombrePrefijo) {
        this.nombrePrefijo = nombrePrefijo;
    }

    
    /**
     * Regresa la descripcion del rol.
     * @return String
     */
    public String getClaveAcceso() {
        return this.claveAcceso;
    }
    public void setClaveAcceso(String claveAcceso) {
        this.claveAcceso = claveAcceso;
    }

}
