package edu.uag.iidis.scec.modelo;

import java.io.Serializable;
import java.util.*;


public class Reporte extends ClaseBase 
        implements Serializable {

    private Long id;
    private String nombre;
    private String descripcion;
    private List usuarios = new ArrayList();

    public Reporte() {
    }

    public Reporte(Long id){
        this.id = id;
    }

    public Reporte(String nombre, String descripcion){
        this.nombre=nombre;
        this.descripcion=descripcion;
    }

    /**
     * Regresa el id del Reporte.
     * @return Long
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Establece el id del Reporte.
     * @return void
     */
    public void setId(Long id) {
        this.id = id;
    }


    /**
     * Regresa el nombre del Reporte.
     * @return String
     */
    public String getNombre() {
        return this.nombre;
    }

    /**
     * Establece el nombre del Reporte.
     * @return void
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
    /**
     * Regresa la descripción del Reporte.
     * @return String
     */
    public String getDescripcion() {
        return this.descripcion;
    }

    /**
     * Establece la descripción del Reporte.
     * @return void
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Regresa los usuarios del Reporte.
     * @return List
     */
    public List getUsuarios() {
        return this.usuarios;
    }

    /**
     * Establece los usuarios del Reporte.
     * @return void
     */
    public void setUsuarios(List usuarios) {
        this.usuarios = usuarios;
    }

    public void addUsuario(Usuario usuario) {
        usuarios.add(usuario);
        usuario.getRoles().add(this);
    }

}
